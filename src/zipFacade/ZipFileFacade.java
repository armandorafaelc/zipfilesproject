package zipFacade;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import zipUtil.ZipUtil;

public class ZipFileFacade implements IZipFacade {
	ZipUtil zip = new ZipUtil();

	@Override
	public void unzipFilesOnAFolder(String pathFile, String pathDestiny, String extension) {
		File file = new File(pathFile);
		if (file.isDirectory()) {
			for (String arquivos : file.list()) {
				if (arquivos.endsWith(extension)) {
					try {
						zip.unzipFiles(pathFile + arquivos.toString(), pathFile);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("Something wrong, the path is alright?");
		}
	}

	@Override
	/*
	 * Name: ZipAFile Class To use this method do you need pass a path with a file
	 * do compress and the name of the file that will be compressed
	 */
	public void zipAFile(String pathFile, String nameFileCompress) throws IOException {
		String sourceFile = pathFile;
		FileOutputStream fileOutputStream = null;
		FileInputStream fileInputStream = null;
		try {
			fileOutputStream = new FileOutputStream(nameFileCompress);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ZipOutputStream zipOut = new ZipOutputStream(fileOutputStream);
		File fileToZip = new File(sourceFile);

		try {
			fileInputStream = new FileInputStream(fileToZip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
		zipOut.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fileInputStream.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		zipOut.close();
		fileInputStream.close();
		fileOutputStream.close();
	}

	@Override
	/*
	 * This method compress a list of files on a unique file .zip, do you must pass how a parameter 
	 * one path with name of file to be compressed, a list of path to files do compress and a password
	 */
	public void zipAFileWithPassword(String pathFile, List<String> filesToAdd, String password) {
		try {
			ZipFile zipFile = new ZipFile(pathFile);
			ArrayList<File> filesToCompress = new ArrayList<File>();
			
			for (String file : filesToAdd) {
				File fileReceive = new File(file);
				filesToCompress.add(fileReceive);
			}

			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); 
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
			parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
			parameters.setPassword(password);

			zipFile.addFiles(filesToCompress, parameters);
		} catch (ZipException e) {
			e.printStackTrace();
		}

	}

}

package zipFacade;

import java.io.IOException;
import java.util.List;

public interface IZipFacade {
	public void unzipFilesOnAFolder(String pathFile, String pathDestiny, String extension);

	public void zipAFile(String pathFile, String nameFileCompress) throws IOException;
	
	public void zipAFileWithPassword(String pathFile, List<String> filesToAdd, String password);
	
}
